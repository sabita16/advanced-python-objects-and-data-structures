d = {'k1': 1, 'k2': 2}

# Iteration over keys, values, and items
for k in d.keys():
    print(k)

for v in d.values():
    print(v)

for item in d.items():
    print(item)


# Viewing keys, values and items
view_key = d.keys()
print(view_key)

d['k3'] = 5
print(d)

view_key = d.keys()
print(view_key)

