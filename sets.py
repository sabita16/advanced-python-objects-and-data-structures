# Different method for the sets.
s = set()

# add
s.add('a')
s.add(1)
print(s)

# clear : removes all elements from the set
s.clear()
print

# copy: makes copy of set and does not affect to the original set
a = {1, 2, 3, 'a', 'b'}
sc = a.copy()
print(sc)
a.add(7)
print(a)
sc.add(5)
print(sc)

# difference: returns the difference of two or more sets. The syntax is:
# set1.difference(set2)
set1 = {1, 2, 5}
set2 = {1, 5, 8}
set3 = {3, 4, 6}
set4 = {1, 2, 5, 8, 9}

print(set1.difference(set2))

# difference_update: returns set1 after removing elements in set2
# difference_update syntax is: set1.difference_update(set2)
set1.difference_update(set3)
print(set1)

# discard: Removes an element from a set if it is a member. If the element is not a member, do nothing.
a.discard(2)
print(a)

# intersection: returns the common elements of two or more given sets
i = set1.intersection(set2)
print(i)

# isdisjoint: if there is not any common element in two or more sets
print(set1.isdisjoint(set2))
print(set2.isdisjoint(set3))

# issubset: checks if another set contains this set.
print(set1.issubset(set3))
print(set1.issubset(set4))

# issuperset: checks if this set contains another set.
print(set4.issuperset(set1))
print(set3.issuperset(set1))

# union: the union/collection of two sets (i.e. all elements that are in either set.)
print(set1.union(set3))

# update: Update a set with the union of itself and others.
set1.update(set3)
print(set1)

# symmetric_difference and symmetric_update
# Return the symmetric difference of two sets as a new set.(i.e. all elements that are in exactly one of the sets.)
# other all elements than the common elements between the sets
print(set1.symmetric_difference(set4))
print(set1.symmetric_difference(set2))






