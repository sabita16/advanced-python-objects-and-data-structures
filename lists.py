

list = [1, 2, 3, 4, 5, 5]

# append: appends/adds an element to the end of a list
list.append(6)
print(list)
# counts: returns the number of times given element occurs in list
print(list.count(10))
print(list.count(2))
print(list.count(5))

# extend: extends list by appending elements from the iterable:

list1 = [5, 6, 7 ]
list1.extend([8, 9])
print(list1)

# index: return the index of element
# If the element is not in the list an error occurred.
print(list.index(3))
# print(list.index(10))

# insert: it places the object at the given index
list1.insert(4, 'hi')
print(list1)

# pop: it pops out/deletes the last element.
# if given index number it pops out that particular element in that index no.
list1.pop(2)
print(list1)

# remove: it removes the first element of what functioned to remove
list3 = [1, 2, 3, 2, 'hello', 5, 2]
list3.remove(2)
print(list3)
list3.remove('hello')
print(list3)
list3.remove(2)
print(list3)

# reverse: reverses a list and it changes the list permanently
list3.reverse()
print(list3)

# sort: it sorts the list in place
list3.sort()
print(list3)
# But, it reverses though sort function is used
list3.sort(reverse=True)
print(list3)


