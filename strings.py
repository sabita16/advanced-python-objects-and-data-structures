topic = "hello. my name is sabita manandhar"
# capitalize the first word in the string
print(topic.capitalize())

# Capitalize/uppercase every letter in the string
print(topic.upper())

# lowercase every letter in the string
print(topic.lower())

# counting the total letter occurred in the string
print(topic.count('a'))

# locating in which index it occurred for the first time
print(topic.find('a'))

# making the string in center between the provided string and provided length
print(topic.center(50, '.'))

## is check method
# checking if all the character in string is alphanumeric/alphabetic/digit
print(topic.isalnum())
print(topic.isalpha())
print(topic.isdigit())

# checks is at all the character in the string is lowercase/uppercase
print(topic.islower())
print(topic.isupper())

# checks in string is its ends with the provided letter
print(topic.endswith('r'))


# return false if there is space in string
print(topic.isspace())

print(topic.istitle())

# Built-in Reg. Expressions
# return the result every thing before and after the given letter in every occurance.
print(topic.split('y'))

# partitions the first part, the provided part and after thing after the separator.
print(topic.partition('y'))


# can be done in different way
print('HELLO'.isupper())





